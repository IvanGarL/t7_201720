package view;

import java.util.Scanner;

import model.data_structures.HashTable;
import model.data_structures.StructuresIterator;
import model.vo.StopVO;
import model.vo.TripVO;
import controller.Controller;

public class STSManagerView {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		while(!fin){
			printMenu();
			
			int option = sc.nextInt();
			int a = 0;
			switch(option){
				case 1:
					if(a > 0 ) 
						{System.out.println("Ya se cargaron los datos una vez."); break;}
					double startTime = System.currentTimeMillis();
					Controller.loadStops();
					Controller.loadTrips();
					Controller.loadST();
					double endTime = System.currentTimeMillis();
					System.out.println("Se han gastado " + (endTime - startTime)/1000 + " segundos.");
					a++;
					break;
				case 2:
					System.out.println("Ingrese el id de la parada.");
					Integer stopId = Integer.parseInt(sc.next());
					startTime = System.currentTimeMillis();
					Controller.tripsByStopAndHour(stopId);
					endTime = System.currentTimeMillis();
					System.out.println("Se han gastado " + (endTime - startTime)/1000 + " segundos.");
					break;
				case 3:
					System.out.println("Ingrese el id de la parada viaje.");
					stopId = Integer.parseInt(sc.next());
					System.out.println("Ingrese la hora inicial:");
					String initialHour = sc.next();
					System.out.println("Ingrese la hora final:");
					String finalHour = sc.next();
					startTime = System.currentTimeMillis();
					Controller.stopsHourRange(stopId, initialHour, finalHour);
					endTime = System.currentTimeMillis();
					System.out.println("Se han gastado " + (endTime - startTime)/1000 + " segundos.");
					break;
				case 4:	
					fin=true;
					sc.close();
					break;
			}
		}
	}

	private static void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 7----------------------");
		System.out.println("1. Cargar Datos");
		System.out.println("2. Encontrar los viajes que pasan por una parada (organizados por hora en RedBlackBST)");
		System.out.println("3. Encontrar las paradas que hacen parte de dos viajes diferentes");
		System.out.println("4. Salir");
		System.out.println("Type the option number for the task, then press enter: (e.g., 1):");
	}
}
