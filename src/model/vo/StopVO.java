package model.vo;


public class StopVO implements Comparable<StopVO>
{
	private int id;
	private int code;
	private String name;
	private String stopDesc;
	private double stopLat;
	private double stopLon;
	private String stopZone;
	private String stopUrl;
	private int locationType;
	private String parentStation;
	private Integer inicio;
	
	public StopVO(int pId, int pCode, String pName, String pStopDesc, double pStopLat, double pStopLon, String pStopZone, String pUrl, int pLocType, String pParent)
	{
		id = pId;
		code = pCode;
		name = pName;
		stopDesc = pStopDesc;
		stopLat = pStopLat;
		stopLon = pStopLon;
		stopZone = pStopZone;
		stopUrl = pUrl;
		locationType = pLocType;
		parentStation = pParent;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStopDesc() {
		return stopDesc;
	}

	public void setStopDesc(String stopDesc) {
		this.stopDesc = stopDesc;
	}

	public double getStopLat() {
		return stopLat;
	}

	public void setStopLat(double stopLat) {
		this.stopLat = stopLat;
	}

	public double getStopLon() {
		return stopLon;
	}

	public void setStopLon(double stopLon) {
		this.stopLon = stopLon;
	}

	public String getStopZone() {
		return stopZone;
	}

	public void setStopZone(String stopZone) {
		this.stopZone = stopZone;
	}

	public String getStopUrl() {
		return stopUrl;
	}

	public void setStopUrl(String stopUrl) {
		this.stopUrl = stopUrl;
	}

	public int getLocationType() {
		return locationType;
	}

	public void setLocationType(int locationType) {
		this.locationType = locationType;
	}

	public String getParentStation() {
		return parentStation;
	}

	public void setParentStation(String parentStation) {
		this.parentStation = parentStation;
	}
	public String toString()
	{
		return id + "";
	}
	
	public void setTime(int i)
	{
		inicio = i;
	}
	
	public int getTime()
	{
		return inicio;
	}
	
	@Override
	public int compareTo(StopVO st) {
		int comp = getId() - st.getId();
		if(comp > 0) return 1;
		else if(comp < 0) return -1;
		else return 0;
	}

	public Integer getKey() {
		// TODO Auto-generated method stub
		return id;
	}

}
