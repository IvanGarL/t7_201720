package src.model.data_structures.tests;

import java.util.Iterator;

import junit.framework.TestCase;
import model.data_structures.Node;
import model.data_structures.StructuresIterator;

public class StructuresIteratorTest extends TestCase
 {
	StructuresIterator<Integer> iterator;
	public void setUp()
	{
		Node<Integer> first = new Node<Integer>((Integer) 1);
		iterator = new StructuresIterator<Integer>(first);
	}
	public void setUp2()
	{
		Node<Integer> first = new Node<Integer>((Integer) 1);
		iterator = new StructuresIterator<Integer>(first);
		first.changeNext(new Node<Integer>((Integer) 2));
		first.getNext().changePrevious(first);
		first.getNext().changeNext(new Node<Integer>((Integer) 3));
		first.getNext().getNext().changePrevious(first.getNext());
	}
	public void testHasPrevious()
	{
		setUp();
		assertFalse("No deberia tener nodo anterior", iterator.hasPrevious());
		
		setUp2();
		iterator.next();
		assertTrue("Deberia tener anterior", iterator.hasPrevious());
	}
	public void testHasNext()
	{
		setUp();
		assertFalse("No deberia tener nodo siguiente", iterator.hasNext());
		
		setUp2();
		assertTrue("Deberia tener siguiente", iterator.hasNext());
	}
	public void testNext()
	{
		setUp();
		iterator.next();
		try {
			iterator.getElement();
			fail("Deberia lanzar excepcion");
		} catch (Exception e) {
			// TODO: handle exception
		}
		setUp2();
		iterator.next();
		assertEquals((Integer) 2, iterator.getElement());
		iterator.next();
		assertEquals((Integer) 3, iterator.getElement());

	}
	public void testPrevious()
	{
		setUp();
		iterator.previous();
		try {
			iterator.getElement();
			fail("Deberia lanzar excepcion");
		} catch (Exception e) {
			// TODO: handle exception
		}
		setUp2();
		iterator.next();
		iterator.next();
		iterator.previous();
		assertEquals((Integer) 2, iterator.getElement());
		iterator.previous();
		assertEquals((Integer) 1, iterator.getElement());
	}
}
